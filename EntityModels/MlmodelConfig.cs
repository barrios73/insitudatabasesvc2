﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseSvc.EntityModels
{
    public partial class MlmodelConfig
    {
        public long Id { get; set; }
        public string Topic { get; set; }
        public int? PreprocessingFileId { get; set; }
        public int? ModelTypeId { get; set; }
        public int? ModelFileId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsActive { get; set; }
    }
}
