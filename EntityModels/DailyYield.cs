﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.EntityModels
{
    public class DailyYield
    {
        public string Process { get; set; }
        public string Product { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TotalCount { get; set; }
        public int OOScount { get; set; }
        public decimal OOSrate { get; set; }
        public decimal Yield { get; set; }
    }
}
