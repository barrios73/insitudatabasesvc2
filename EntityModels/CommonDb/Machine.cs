﻿namespace DatabaseSvc.EntityModels.CommonDb
{
    public class Machine
    {
        public int Id { get; set; }
        public string MachineId { get; set; }
        public int ModuleId { get; set; }
        public int ProductionLineId { get; set; }
    }
}
