﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DatabaseSvc.EntityModels.CommonDb
{
    public partial class CommonDbContext : DbContext
    {
        private readonly ILogger<CommonDbContext> _logger;
        private readonly IConfiguration _configuration;

        //public CommonDbContext()
        //{

        //}

        public CommonDbContext(DbContextOptions<CommonDbContext> options, ILogger<CommonDbContext> logger, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public virtual DbSet<Machine> Machines { get; set; }
        public virtual DbSet<Process> Processes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductionLine> ProductionLines { get; set; }
        public virtual DbSet<Module> Modules { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //    #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        //optionsBuilder.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = CommonDb; User ID = InSitu; Password = insituanomaly");
        //        //string url = _configuration["Database:Url"];
        //        //optionsBuilder.UseSqlServer(url)
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Machine>(entity =>
            {
                entity.ToTable("Machine");

                entity.Property(e => e.MachineId).HasMaxLength(50);

                entity.Property(e => e.ModuleId);

                entity.Property(e => e.ProductionLineId);
            });

            modelBuilder.Entity<Process>(entity =>
            {
                entity.ToTable("Process");

                entity.Property(e => e.ProcessId).HasMaxLength(50);

                entity.Property(e => e.ProcessName).HasMaxLength(150);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.Property(e => e.ProductId).HasMaxLength(150);

                entity.Property(e => e.ProductName).HasMaxLength(150);
            });


            modelBuilder.Entity<ProductionLine>(entity =>
            {
                entity.ToTable("ProductionLine");

                entity.Property(e => e.Line).HasMaxLength(100);
            });


            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("Module");

                entity.Property(e => e.ModuleName).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
