﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DatabaseSvc.DataAccess.Interface;
using DatabaseSvc.EntityModels;
using DatabaseSvc.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DatabaseSvc.EntityModels.CommonDb;

namespace DatabaseSvc.DataAccess.Implementation
{
    public class PredResultSvc: IPredResultSvc
    {
        private readonly PredResultRepo _repo;
        private readonly CommonDbRepo _commonDbRepo;
        //private readonly RabbitMQPublisher _publisher;
        private readonly IHttpClientFactory _httpClientFactory;
        private string currentWO = "";
        private bool sendInprocessDispositionMsg = false;


        public PredResultSvc(PredResultRepo repo, IHttpClientFactory httpClientFactory, CommonDbRepo commonDbRepo)
        {
            _repo = repo;
            _commonDbRepo = commonDbRepo;
            this._httpClientFactory = httpClientFactory;
        }

        public async Task<Message<int>> SavePredictionResult(PredictionResult result)
        {            
            Message<int> msg = await SaveResult(result);

            //for CPPS
            if (ConfigSettings.CPPS == true)
            {
                if (msg.HasError == false)
                {
                    if (result.WOId != currentWO)
                    {
                        sendInprocessDispositionMsg = false;
                        currentWO = result.WOId;
                    }

                    if (result.PredictResult.ToUpper() == "FAIL" && sendInprocessDispositionMsg == false)
                    {
                        //get prediction results by WO and process
                        //if more than scrap limit, send msg to tracking and set
                


                        await GetWODisposition(result);
                        sendInprocessDispositionMsg = true;
                    }
                }
            }


            return msg;
        }


        public async Task<Message<int>> SaveResult(PredictionResult result)
        {
            Message<int> msg = new Message<int>();
            try
            {
                PredictionResult res = CheckIfResultOOS(result);

                int rowsSaved = await _repo.SaveResult(res);
                msg.HasError = false;
                msg.Data = rowsSaved;
                msg.Msg = "No errros";
            }
            catch (Exception ex)
            {
                msg.HasError = true;
                msg.Data = 0;
                msg.Msg = $"Error in saving prediction result: {ex.Message}";
            }

            return msg;
        }

        public async Task<Message<int>> SaveRawData(PredictionRawData data)
        {
            Message<int> resp = new Message<int>();
            try
            {
                int result = await _repo.SaveRawData(data);

                resp.HasError = false;
                resp.Msg = "No errors";
                resp.Data = result;

            }
            catch (Exception ex)
            {
                resp.HasError = true;

                resp.Msg = ex.Message;
                resp.Data = 0;
            }
            return resp;
        }


        public async Task<Message<List<DtoPredictionResult>>> GetCurrentPredictionResult(DateTime dashboardDate, int? moduleId, int? prodLineId, int? machineKeyId)
        {
             Message<List<DtoPredictionResult>> result = new Message<List<DtoPredictionResult>>();

            try
            {
                List<TbResult> msg = await _repo.GetCurrentPredictionResult(dashboardDate);
                List<Machine> machineList = await _commonDbRepo.GetMachines();
                List<Module> moduleList = await _commonDbRepo.GetModules();
                List<ProductionLine> prodLineList = await _commonDbRepo.GetProductionLines();

                List<DtoPredictionResult> resultList =
                 (
                     from res in msg
                     join machine in machineList
                     on res.MachineId equals machine.MachineId into resMachList
                     from resMach in resMachList.DefaultIfEmpty()
                     where resMach != null

                     join prodline in prodLineList
                     on resMach.ProductionLineId equals prodline.Id into resMachModuleProdlineList
                     from resMachModuleProdline in resMachModuleProdlineList.DefaultIfEmpty()


                     join module in moduleList
                     on resMach.ModuleId equals module.Id into resMachModuleList
                     from resMachModule in resMachModuleList.DefaultIfEmpty()


                     select new DtoPredictionResult
                     {
                         Topic = res.Topic ?? string.Empty,
                         MachineKeyId = resMach.Id,
                         MachineId = res.MachineId ?? string.Empty,
                         ModuleId = resMachModule != null ? resMachModule.Id : 0,
                         Module = resMachModule != null ? resMachModule.ModuleName : string.Empty,
                         ProductionLineId = resMachModuleProdline != null ? resMachModuleProdline.Id : 0,
                         ProductionLine = resMachModuleProdline != null ? resMachModuleProdline.Line : string.Empty,
                         ProductId = res.ProductId ?? string.Empty,
                         ProcessId = res.ProcessId ?? string.Empty,
                         WOId = res.WOId ?? string.Empty,
                         WorkpieceId = res.WorkpieceId ?? string.Empty,
                         PredictResult = res.Result,
                         CreatedDateTime = res.CreatedDateTime,
                         IsOOS = res.IsOos

                     }
                 ).Where(x => (x.ModuleId == moduleId || moduleId == null)
                     && (x.ProductionLineId == prodLineId || prodLineId == null)
                     && (x.MachineKeyId == machineKeyId || machineKeyId == null)
                 )
                 .Concat(
                     from res in msg
                     join machine in machineList
                     on res.MachineId equals machine.MachineId into resMachList
                     from resMach in resMachList.DefaultIfEmpty()
                     where resMach == null
                     select new DtoPredictionResult
                     {
                         Topic = res.Topic ?? string.Empty,
                         MachineKeyId = 0,
                         MachineId = res.MachineId ?? string.Empty,
                         ModuleId = 0,
                         Module = res.MachineId + " (undefined)",
                         ProductionLineId = 0,
                         ProductionLine = res.MachineId + " (undefined)",
                         ProductId = res.ProductId ?? string.Empty,
                         ProcessId = res.ProcessId ?? string.Empty,
                         WOId = res.WOId ?? string.Empty,
                         WorkpieceId = res.WorkpieceId ?? string.Empty,
                         PredictResult = res.Result,
                         CreatedDateTime = res.CreatedDateTime,
                         IsOOS = res.IsOos
                     }).Where(x => (x.ModuleId == moduleId || moduleId == null)
                         && (x.ProductionLineId == prodLineId || prodLineId == null)
                         && (x.MachineKeyId == machineKeyId || machineKeyId == null)
                 ).ToList<DtoPredictionResult>();

                result.HasError = false;
                result.Msg = "No errors";
                result.Data = resultList;
            }
            catch(Exception ex)
            {
                result.HasError = true;
                result.Msg = $"Errors getting current prediction results: {ex.Message}";
                result.Data = new List<DtoPredictionResult>();
            }

            return result;
        }


        private PredictionResult CheckIfResultOOS(PredictionResult result)
        {
            try
            {
                if (result.OutputType == "numerical")
                {
                    double numResult;
                    if (Double.TryParse(result.PredictResult, out numResult))
                    {
                        if (numResult < result.MinValueOutputParam || numResult > result.MaxValueOutputParam)
                        {
                            result.IsOOS = true;
                        }
                        else
                        {
                            result.IsOOS = false;
                        }
                    }
                }
                else if (result.OutputType == "categorical")
                {
                    List<string> actualLabels = result.ActualLabel.Split(",").ToList<string>();
                    List<string> isOOSLabels = result.IsOOSLabel.Split(",").ToList<string>();

                    if (actualLabels.IndexOf(result.PredictResult) != -1)
                    {
                        result.IsOOS = bool.Parse(isOOSLabels[actualLabels.IndexOf(result.PredictResult)]);
                    }
                    else
                    {
                        result.IsOOS = false;
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public async Task<Message<List<TbResult>>> GetHistPredictionResult(SearchCriteria criteria)
        {
            Message<List<TbResult>> msg = new Message<List<TbResult>>();

            try
            {
                List<TbResult> result = await _repo.GetHistPredictionResults(criteria);
                msg.HasError = false;
                msg.Msg = "No errors";
                msg.Data = result;

                return msg;
            }
            catch (Exception ex)
            {
                msg.HasError = true;
                msg.Msg = "Errors in getting historical prediction results";
                msg.Data = null;

                return msg;
            }
        }


        public Message<List<DailyYield>> GetDailyOOSTrend(SearchCriteria criteria)
        {
            Message<DataTable> msg = _repo.GetDailyOOSTrend(criteria);

            List<DailyYield> dyList = new List<DailyYield>();
            foreach(DataRow row in msg.Data.Rows)
            {
                DailyYield dy = new DailyYield();
                dy.Process = row.ItemArray[0].ToString();
                dy.Product = row.ItemArray[1].ToString();
                dy.CreatedDate = DateTime.Parse(row.ItemArray[2].ToString());
                dy.TotalCount = Convert.ToInt32(row.ItemArray[3]);
                dy.OOScount = Int32.Parse(row.ItemArray[4].ToString());
                dy.OOSrate = Decimal.Parse(row.ItemArray[5].ToString());
                dy.Yield = Decimal.Parse(row.ItemArray[6].ToString());
                dyList.Add(dy);
            }

            Message<List<DailyYield>> result = new Message<List<DailyYield>>();
            result.HasError = msg.HasError;
            result.Msg = msg.Msg;
            result.Data = dyList;

            return result;
        }

        public Message<List<PassFailResultWO>> GetPredictionResultsByWO(string workOrderNo, string processId)
        {
            Message<List<TbResult>> msg = _repo.GetPredictionResultsByWO(workOrderNo, processId);


            List<PassFailResultWO> passFailResultList = new List<PassFailResultWO>();
            foreach (TbResult res in msg.Data)
            {
                PassFailResultWO resultWO = new PassFailResultWO();
                resultWO.ProcessId = res.ProcessId;
                resultWO.ProductId = res.ProductId;
                resultWO.WorkpieceId = res.WorkpieceId;
                if (res.IsOos == null || res.IsOos == false)
                {
                    resultWO.OOS = 0;
                }
                else if (res.IsOos == true)
                {
                    resultWO.OOS = 1;
                }
                passFailResultList.Add(resultWO);
            }

            Message<List<PassFailResultWO>> result = new Message<List<PassFailResultWO>>();
            result.HasError = msg.HasError;
            result.Msg = msg.Msg;
            result.Data = passFailResultList;

            return result;
           
        }


        private async Task<WOInfo> GetWODisposition(PredictionResult result)
        {
            //string baseUrl = Environment.GetEnvironmentVariable("BASE_URL");

            string baseUrl = "http://192.168.137.1:6001";
            Uri uri = new Uri(baseUrl + "/api/Disposition/GetWODisposition/");

            var httpClient = _httpClientFactory.CreateClient();

            WOInfo woinfo = new WOInfo();
            woinfo.WOId = result.WOId;
            woinfo.ProductId = result.ProductId;
            woinfo.ProcessId = result.ProcessId;
            woinfo.MachineId = result.MachineId;
            woinfo.TotalQty = 1;    //1 is just arbitary value so there is no error at API side

            var wo = JsonSerializer.Serialize(woinfo);
            var requestContent = new StringContent(wo, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(uri, requestContent);

           response.EnsureSuccessStatusCode();


            using var content =  await response.Content.ReadAsStreamAsync();

           WOInfo newwo = await JsonSerializer.DeserializeAsync<WOInfo>(content);

            return newwo;
        }


        public async Task<Message<List<SearchRawDataResult>>> GetRawData(SearchRawDataCriteria criteria)
        {
            Message<List<SearchRawDataResult>> resp = new Message<List<SearchRawDataResult>>();
            try
            {
                List<RawData> rawDataList = await _repo.GetRawData(criteria);
                List<TbResult> rawDataBatchInfo = await _repo.GetRawDataBatchInfo(criteria);

                if (rawDataList.Count > 0 && rawDataBatchInfo.Count > 0)
                {
                    List<SearchRawDataResult> result =

                    (from batchinfo in rawDataBatchInfo
                     join rawdata in rawDataList on batchinfo.CorrelationId equals rawdata.CorrelationId
                     select new SearchRawDataResult
                     {
                         Topic = batchinfo?.Topic ?? string.Empty,
                         MachineId = batchinfo?.MachineId ?? string.Empty,
                         ProductId = batchinfo.ProductId ?? string.Empty,
                         ProcessId = batchinfo.ProcessId ?? string.Empty,
                         WOId = batchinfo.WOId ?? string.Empty,
                         CorrelationId = batchinfo.CorrelationId ?? string.Empty,
                         ParameterName = rawdata?.ParameterName ?? string.Empty,
                         Data = rawdata?.Data ?? string.Empty,
                         Result = batchinfo.Result ?? string.Empty,
                         CreatedDateTime = rawdata?.CreatedDateTime ?? DateTime.MinValue
                     }).ToList<SearchRawDataResult>();


                    //(from rawdata in rawDataList
                    // join batchinfo in rawDataBatchInfo on rawdata.CorrelationId equals batchinfo.CorrelationId 
                    // //into finalBatchInfoList from finalBatchInfo in finalBatchInfoList.DefaultIfEmpty()
                    // select new SearchRawDataResult
                    // {
                    //     Topic = batchinfo?.Topic ?? string.Empty,
                    //     MachineId = batchinfo?.MachineId ?? string.Empty,
                    //     ProductId = batchinfo.ProductId ?? string.Empty,
                    //     ProcessId = batchinfo.ProcessId ?? string.Empty,
                    //     WOId = batchinfo.WOId ?? string.Empty,
                    //     CorrelationId = batchinfo.CorrelationId ?? string.Empty,
                    //     ParameterName = rawdata?.ParameterName ?? string.Empty,
                    //     Data = rawdata?.Data ?? string.Empty,
                    //     Result = batchinfo.Result ?? string.Empty,
                    //     CreatedDateTime = rawdata?.CreatedDateTime ?? DateTime.MinValue
                    // }).ToList<SearchRawDataResult>();

                    resp.HasError = false;
                    resp.Msg = "No errors";
                    resp.Data = result;
                }
                else
                {
                    resp.HasError = true;
                    resp.Msg = "No raw data found based on the search criteria";
                    resp.Data = null;
                }
            }
            catch (Exception ex)
            {
                resp.HasError = true;
                resp.Msg = ex.Message;
                resp.Data = null;
            }

            return resp;

        }

    }
}
