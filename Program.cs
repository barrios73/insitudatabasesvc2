using Microsoft.EntityFrameworkCore;
using DatabaseSvc.DataAccess.Implementation;
using DatabaseSvc;
using DatabaseSvc.EntityModels;
using DatabaseSvc.EntityModels.CommonDb;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


builder.Services.AddDbContext<InSituDbContext>(options =>
{
    options.UseSqlServer(Environment.GetEnvironmentVariable("INSITUDB_URL"));
    //options.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = InSituDb_PH; User ID = InSitu; Password = insituanomaly; Encrypt = false");                
});


builder.Services.AddDbContext<CommonDbContext>(options =>
{
    options.UseSqlServer(Environment.GetEnvironmentVariable("COMMONDB_URL"));
    //options.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = CommonDb_PH; User ID = InSitu; Password = insituanomaly; Encrypt = false");
});


builder.Services.AddTransient<PredResultSvc>();
builder.Services.AddTransient<PredResultRepo>();
builder.Services.AddTransient<CommonDbRepo>();
builder.Services.AddHostedService<RabbitMQWorkerService>();
builder.Services.AddHttpClient();

builder.Services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
{
    builder
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowAnyOrigin()
    .AllowCredentials()
    .WithOrigins("http://localhost:4200", "http://localhost", "http://192.168.137.1", "http://172.20.115.26");
}));


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicy");
app.UseRouting();
app.UseAuthorization();

app.MapControllers();

app.Run();
