﻿namespace DatabaseSvc.Models
{
    public class SearchRawDataCriteria
    {
        public string Topic { get; set; }
        public string ParameterName { get; set; }
        public string MachineId { get; set; }
        public string ProductId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
