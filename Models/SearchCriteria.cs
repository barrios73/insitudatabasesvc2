﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.Models
{
    public class SearchCriteria
    {
        public string? Process { get; set; }
        public string? Product { get; set; }
        public string? Machine { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
