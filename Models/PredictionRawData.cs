﻿namespace DatabaseSvc.Models
{
    public class PredictionRawData
    {
        public string CorrelationId { get; set; }
        public string[] InputParamNames { get; set; }
        public double[][] Data { get; set; }
    }
}
