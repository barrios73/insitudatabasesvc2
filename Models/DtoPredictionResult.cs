﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.Models
{
    public class DtoPredictionResult
    {
        public string Topic { get; set; }
        public int MachineKeyId { get; set; }
        public string MachineId { get; set; }
        public int ModuleId { get; set; }
        public string Module { get; set; }
        public int ProductionLineId { get; set; }
        public string ProductionLine { get; set; }
        public string ProductId { get; set; }
        public string ProcessId { get; set; }
        public string WOId { get; set; }
        public string WorkpieceId { get; set; }
        public string PredictResult { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public bool? IsOOS { get; set; }
    }
}
