﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.Models
{
    public class Message<T>
    {
        public bool HasError { get; set; }
        public string Msg { get; set; }
        public T Data { get; set; }
    }
}
