﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using DatabaseSvc.DataAccess.Implementation;
using DatabaseSvc.Models;
using DatabaseSvc.EntityModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace DatabaseSvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]   
    //[Authorize]
    public class DatabaseController : Controller
    {
        private readonly PredResultSvc _dBSvc;
        private readonly ILogger<DatabaseController> _logger;

        public DatabaseController(ILogger<DatabaseController> logger, PredResultSvc dbSvc)
        {
            _dBSvc = dbSvc;
            _logger = logger;
        }

        
        [HttpGet("Summary/GetCurrentPredictionResult")]
        //[Authorize(Policy = "getcurpredictionresult")]
        public async Task<IActionResult> Get(string? moduleId = null, string? prodlineId = null, string? machineKeyId = null)
        {
            int? searchModuleId;
            int? searchProdlineId;
            int? searchMachineKeyId;

            searchModuleId = moduleId != null ? int.Parse(moduleId) : null;
            searchProdlineId = prodlineId != null ? int.Parse(prodlineId) : null;
            searchMachineKeyId = machineKeyId != null ? int.Parse(machineKeyId) : null;


            //set the dashboard start time here, by right should get from config dB
            DateTime curDate = DateTime.Now;
            int dashboardYear = curDate.Year;
            int dashboardMonth = curDate.Month;
            int dashboardDay = curDate.Day;
            int dashboardHr = Int32.Parse("00");    //was 08, should get from db
            int dashboardMin = Int32.Parse("00");   //should get from db
            int dashboardSec = 0;
            DateTime dashboardDate = new DateTime(dashboardYear, dashboardMonth, dashboardDay, dashboardHr, dashboardMin, dashboardSec);


            DateTime searchStartDate;
            //Since SG time is UCT+8, 00:00:00 midnight is 16:00:00 UTC which is 16*60*60=57600
            if (curDate.Hour * 60 * 60 + curDate.Minute * 60 + curDate.Second >= 57600 &&
                curDate.Hour * 60 * 60 + curDate.Minute * 60 + curDate.Second <= 86400)
            {
                _logger.LogInformation("Cur Hr: " + curDate.Hour);
                _logger.LogInformation("Cur Min: " + curDate.Minute);
                _logger.LogInformation("Cur Sec: " + curDate.Second);


                DateTime previousDate = DateTime.Now.AddDays(-1);

                int startYear = previousDate.Year;
                int startMonth = previousDate.Month;
                int startDay = previousDate.Day;
                int startHr = dashboardDate.Hour;    //was 00
                int startMin = dashboardDate.Minute;
                int startSec = 0;
                searchStartDate = new DateTime(startYear, startMonth, startDay, startHr, startMin, startSec);
                //getPredictionResult(dashboardTime)
            }
            else
            {
                _logger.LogInformation("Cur Hr: " + curDate.Hour);
                _logger.LogInformation("Cur Min: " + curDate.Minute);
                _logger.LogInformation("Cur Sec: " + curDate.Second);

                int startYear = DateTime.Now.Year;
                int startMonth = DateTime.Now.Month;
                int startDay = DateTime.Now.Day;
                int startHr = dashboardDate.Hour;    //was 00
                int startMin = dashboardDate.Minute;
                int startSec = 0;
                searchStartDate = new DateTime(startYear, startMonth, startDay, startHr, startMin, startSec);
            }


            Message<List<DtoPredictionResult>> msg = await _dBSvc.GetCurrentPredictionResult(searchStartDate, searchModuleId, searchProdlineId, searchMachineKeyId);

            ObjectResult result = new ObjectResult(msg);

            if (msg.HasError == false)
            {
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status200OK;
            }
            else if (msg.HasError == true)
            {
                _logger.LogError("DatabaseSvc: error in GetCurrentPredictionResult - " + msg.Msg);
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return result;
        }


        [HttpPost("History/GetDailyOOSTrend")]
        public IActionResult GetDailyOOSTrend(SearchCriteria criteria)
        {
            if (criteria.Process == "") criteria.Process = null;
            if (criteria.Product == "") criteria.Product = null;
            if (criteria.Machine == "") criteria.Machine = null;

            Message<List<DailyYield>> msg = _dBSvc.GetDailyOOSTrend(criteria);         

            ObjectResult result = new ObjectResult(msg);

            if (msg.HasError == false)
            {
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status200OK;
            }
            else if (msg.HasError == true)
            {
                _logger.LogError("DatabaseSvc: error in GetDailyYield - " + msg.Msg);
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return result;

        }


        [HttpGet("Quality/PredictionResultsByWO/{WorkOrderNo}/{ProcessId}")]
        public ActionResult<List<PassFailResultWO>> GetPredictionResultsByWO(string workOrderNo, string processId)
        {

            Message<List<PassFailResultWO>> resp = _dBSvc.GetPredictionResultsByWO(workOrderNo, processId);

            //if (resp.HasError == false)
            //{
            //    return Ok(resp);
            //}
            //else
            //{
            //    _logger.LogError("DatabaseSvc: error in GetPredictionResultsByWO - " + resp.Msg);
            //    return StatusCode(StatusCodes.Status500InternalServerError, resp.Msg);
            //}


            ObjectResult result = new ObjectResult(resp);

            if (resp.HasError == false)
            {
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status200OK;
            }
            else if (resp.HasError == true)
            {
                _logger.LogError("DatabaseSvc: error in GetPredictionResultsByWO - " + resp.Msg);
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return result;
        }


        [HttpPost("GetRawData")]
        public async Task<ActionResult<List<SearchRawDataResult>>> GetRawData(SearchRawDataCriteria request)
        {
            if (request.MachineId == "") request.MachineId = null;
            if (request.ProductId == "") request.ProductId = null;
            if (request.ParameterName == "") request.ParameterName = null;

            Message<List<SearchRawDataResult>> resp = await _dBSvc.GetRawData(request);

            ObjectResult result = new ObjectResult(resp);

            if (resp.HasError == false)
            {
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status200OK;
            }
            else if (resp.HasError == true)
            {
                _logger.LogError("DatabaseSvc: error in GetPredictionResultsByWO - " + resp.Msg);
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return result;
        }


      
    }
}
